using System;
using System.Collections.Generic;
using UnityEngine;

public class MoonManager : MonoBehaviour {
    public List<GravityBody> objects;

    [SerializeField]
    private GameObject displayPrefab;

    private GameObject[] _displays;

    private void Awake() {
        _displays = new GameObject[0];
    }

    public void FixedUpdate() {
        for (var i = 0; i < objects.Count; i++) {
            var moon = objects[i];

            var time = Time.fixedTime * moon.timeScale / 10;

            var x = Mathf.Cos(time) * moon.distance;
            var y = 0;
            var z = Mathf.Sin(time) * moon.distance;

            var pos = new Vector3(x, y, z);

            var axisTilt = Quaternion.AngleAxis(moon.tilt * Mathf.Rad2Deg, Vector3.forward);
            var axisRot = Quaternion.AngleAxis(moon.rotation * Mathf.Rad2Deg, Vector3.up);

            pos = axisTilt * pos;
            pos = axisRot * pos;
            
            moon.position = pos;

            objects[i] = moon;
        }
    }

    private void Update() {
        if (_displays.Length != objects.Count) {
            foreach (var display in _displays) {
                Destroy(display);
            }

            _displays = new GameObject[objects.Count];

            for (var i = 0; i < _displays.Length; i++) {
                _displays[i] = Instantiate(displayPrefab, transform);
            }
        }

        for (var i = 0; i < objects.Count; i++) {
            _displays[i].transform.position = objects[i].position;
            _displays[i].transform.localScale = Vector3.one * objects[i].size;
        }
    }
}

[Serializable]
public struct GravityBody {
    public Vector3 position;

    public float distance;
    public float timeScale;

    public float size;

    [Range(0, Mathf.PI / 2)] public float tilt;
    [Range(0, Mathf.PI * 2)] public float rotation;
}