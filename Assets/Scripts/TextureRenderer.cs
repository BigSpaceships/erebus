using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class TextureRenderer : MonoBehaviour {
    public RenderTexture frequencyTextureBuffer;
    public RenderTexture frequencyTexture;
    public RenderTexture waveDataTexture;
    public ComputeBuffer paramsBuffer;
    public ComputeShader shader;

    public RenderTexture frequencyTextureTime;
    public ComputeShader timeDependantShader;

    public RenderTexture dxDz;
    public RenderTexture dyDxz;
    public RenderTexture dyxDyz;
    public RenderTexture dxxDzz;
    public RenderTexture buffer;

    public FFT fft;
    public ComputeShader fftShader;

    public RenderTexture displacement;
    public RenderTexture derivatives;
    public RenderTexture turbulence;
    public ComputeShader textureMerger;

    public GameObject display;

    public int textureSize;

    public enum RenderMode {
        Frequency,
        WaveData,
        TimeFrequency,
        D1,
        Displacement,
        Derivatives,
    }

    public RenderMode mode;

    public bool regenSettings;

    [Header("Water Settings")] // gah more comments
    [SerializeField] WavesSettings wavesSettings;
    [SerializeField] private float lengthScale;
    [SerializeField] private float lambda; // what does this do? good question

    [SerializeField] private Texture2D noiseTexture;

    public float GetGaussianRandom() {
        return Mathf.Cos(2 * Mathf.PI * Random.value) * Mathf.Sqrt(-2 * Mathf.Log(Random.value));
    }

    public void GenerateGaussianTexture() {
        noiseTexture = new Texture2D(textureSize, textureSize, TextureFormat.RGFloat, false, true);
        noiseTexture.filterMode = FilterMode.Point;

        for (int i = 0; i < textureSize; i++) {
            for (int j = 0; j < textureSize; j++) {
                noiseTexture.SetPixel(i, j, new Vector4(GetGaussianRandom(), GetGaussianRandom()));
            }
        }

        noiseTexture.Apply();
    }

    public void Generate() {
        textureSize = Mathf.NextPowerOfTwo(textureSize);

        fft = new FFT(textureSize, fftShader);

        frequencyTexture = CreateRenderTexture(textureSize, RenderTextureFormat.ARGBFloat);
        waveDataTexture = CreateRenderTexture(textureSize, RenderTextureFormat.ARGBFloat);
        frequencyTextureBuffer = CreateRenderTexture(textureSize);
        frequencyTextureTime = CreateRenderTexture(textureSize, RenderTextureFormat.ARGBFloat);
        paramsBuffer = new ComputeBuffer(2, 8 * sizeof(float));

        dxDz = CreateRenderTexture(textureSize);
        dyDxz = CreateRenderTexture(textureSize);
        dyxDyz = CreateRenderTexture(textureSize);
        dxxDzz = CreateRenderTexture(textureSize);

        buffer = CreateRenderTexture(textureSize);

        displacement = CreateRenderTexture(textureSize, RenderTextureFormat.ARGBFloat);
        derivatives = CreateRenderTexture(textureSize, RenderTextureFormat.ARGBFloat, true);
        turbulence = CreateRenderTexture(textureSize, RenderTextureFormat.ARGBFloat, true);

        Debug.Log(textureSize);
        shader.SetFloat("LengthScale", lengthScale);
        shader.SetFloat("CutoffHigh", 2.217f);
        shader.SetFloat("CutoffLow", 0.0001f);
        wavesSettings.SetParametersToShader(shader, 0, paramsBuffer);
        shader.SetInt("Size", textureSize);
        
        shader.SetTexture(0, "H0K", frequencyTextureBuffer);
        shader.SetTexture(0, "WavesData", waveDataTexture);
        shader.SetTexture(0, "Noise", noiseTexture);

        shader.Dispatch(0, textureSize / 8, textureSize / 8, 1);

        shader.SetTexture(1, "H0", frequencyTexture);
        shader.SetTexture(1, "H0K", frequencyTextureBuffer);
        shader.Dispatch(1, textureSize / 8, textureSize / 8, 1);
    }

    private void OnDestroy() {
        paramsBuffer?.Release();
    }

    public static RenderTexture CreateRenderTexture(int size, RenderTextureFormat format = RenderTextureFormat.RGFloat,
        bool useMips = false) {
        RenderTexture rt = new RenderTexture(size, size, 0,
            format, RenderTextureReadWrite.Linear);
        rt.useMipMap = useMips;
        rt.autoGenerateMips = false;
        rt.anisoLevel = 6;
        rt.filterMode = FilterMode.Trilinear;
        rt.wrapMode = TextureWrapMode.Repeat;
        rt.enableRandomWrite = true;
        rt.Create();
        return rt;
    }

    public void RenderDisplay() {
        RenderTexture texture = displacement;
        switch (mode) {
            case RenderMode.Frequency:
                texture = frequencyTexture;
                break;
            case RenderMode.WaveData:
                texture = waveDataTexture;
                break;
            case RenderMode.TimeFrequency:
                texture = frequencyTextureTime;
                break;
            case RenderMode.D1:
                texture = dxDz;
                break;
            case RenderMode.Displacement:
                texture = displacement;
                break;
            case RenderMode.Derivatives:
                texture = derivatives;
                break;
        }

        display.GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_MainTex", texture);
    }

    private void Awake() {
        Generate();
    }

    private void Update() {
        if (regenSettings) {
            Generate();
        }

        timeDependantShader.SetTexture(0, "H0", frequencyTexture);
        timeDependantShader.SetTexture(0, "WaveData", waveDataTexture);
        timeDependantShader.SetTexture(0, "CombinedAmplitude", frequencyTextureTime);

        timeDependantShader.SetTexture(0, "Dx_Dz", dxDz);
        timeDependantShader.SetTexture(0, "Dy_Dxz", dyDxz);
        timeDependantShader.SetTexture(0, "Dyx_Dyz", dyxDyz);
        timeDependantShader.SetTexture(0, "Dxx_Dzz", dxxDzz);

        timeDependantShader.SetFloat("Time", Time.time);

        timeDependantShader.Dispatch(0, textureSize / 8, textureSize / 8, 1);

        fft.IFFT2D(dxDz, buffer, true, false, true);
        fft.IFFT2D(dyDxz, buffer, true, false, true);
        fft.IFFT2D(dyxDyz, buffer, true, false, true);
        fft.IFFT2D(dxxDzz, buffer, true, false, true);

        textureMerger.SetTexture(0, "Dx_Dz", dxDz);
        textureMerger.SetTexture(0, "Dy_Dxz", dyDxz);
        textureMerger.SetTexture(0, "Dyx_Dyz", dyxDyz);
        textureMerger.SetTexture(0, "Dxx_Dzz", dxxDzz);
        textureMerger.SetTexture(0, "Displacement", displacement);
        textureMerger.SetTexture(0, "Derivatives", derivatives);
        textureMerger.SetTexture(0, "Turbulence", turbulence);
        textureMerger.SetFloat("Lambda", lambda);
        textureMerger.Dispatch(0, textureSize / 8, textureSize / 8, 1);
        
        derivatives.GenerateMips();
        turbulence.GenerateMips();
    }
}