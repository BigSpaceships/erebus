using UnityEngine;

public class FlatChunk {
    private int _vertexCount;
    private Mesh _mesh;

    private Vector3[] _vertices;
    private Vector3[] _normals;
    private Vector2[] _uvs;
    private int[] _tris;

    public GameObject gameObject;

    public MeshRenderer meshRenderer;

    // have a size variable to determine how big the chunk is. the biggest be like 4 or 8 or something like that 
    // have pass in the size, face, and where on the face it is
    // do math to make face :D

    public FlatChunk(int vertexCount, float size, Vector2Int pos) {
        _vertexCount = vertexCount;

        GenerateMesh(vertexCount, size, pos);
        CreateObject();
    }

    private void GenerateMesh(int vertexCount, float size, Vector2Int pos) {
        _mesh = new Mesh();

        _vertices = new Vector3[vertexCount * vertexCount];
        _tris = new int[(vertexCount - 1) * vertexCount * vertexCount *
                        6]; // ngl i don't get this but it's also what makes sense when assigning them

        _uvs = new Vector2[vertexCount * vertexCount];
        _normals = new Vector3[vertexCount * vertexCount];

        var corner1 = new Vector3(pos.x * size, 0, pos.y * size);
        var increment2 = Vector3.right * size / vertexCount;
        var increment1 = Vector3.forward * size / vertexCount;

        // var sizeIncrement1 = increment1 * 2 / Mathf.Pow(2, size);
        // var sizeIncrement2 = increment2 * 2 / Mathf.Pow(2, size);

        // corner1 += sizeIncrement1 * pos.x + sizeIncrement2 * pos.y;

        increment1 = increment1 / (vertexCount - 1) * 2;
        increment2 = increment2 / (vertexCount - 1) * 2;

        for (var i = 0; i < vertexCount; i++) {
            for (var j = 0; j < vertexCount; j++) {
                var vertex = corner1 + increment1 * i + increment2 * j;

                _normals[PosToIndex(i, j)] = Vector3.up;
                _vertices[PosToIndex(i, j)] = vertex;

                _uvs[PosToIndex(i, j)] = new Vector2(i / (vertexCount - 1f), j / (vertexCount - 1f));
            }
        }

        for (var i = 0; i < vertexCount - 1; i++) {
            for (var j = 0; j < vertexCount - 1; j++) {
                var startingIndex = (j * vertexCount + i) * 6;

                _tris[startingIndex] = PosToIndex(i, j);
                _tris[startingIndex + 1] = PosToIndex(i + 1, j);
                _tris[startingIndex + 2] = PosToIndex(i, j + 1);

                _tris[startingIndex + 3] = PosToIndex(i + 1, j);
                _tris[startingIndex + 4] = PosToIndex(i + 1, j + 1);
                _tris[startingIndex + 5] = PosToIndex(i, j + 1);
            }
        }

        _mesh.SetVertices(_vertices);
        _mesh.SetNormals(_normals);
        _mesh.SetUVs(0, _uvs);
        _mesh.SetTriangles(_tris, 0);
    }

    private void CreateObject() {
        gameObject = new GameObject("Chunk");

        gameObject.AddComponent<MeshFilter>();
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        gameObject.AddComponent<MeshCollider>();

        gameObject.GetComponent<MeshFilter>().mesh = _mesh;
        gameObject.GetComponent<MeshCollider>().sharedMesh = _mesh;
    }

    private int PosToIndex(int x, int y) {
        return y * _vertexCount + x;
    }
}