using UnityEngine;

public class Chunk {
    private int _vertexCount;
    private Mesh _mesh;

    private Vector3[] _vertices;
    private Vector3[] _normals;
    private Vector2[] _uvs;
    private int[] _tris;

    public GameObject gameObject;

    public MeshRenderer meshRenderer;

    // have a size variable to determine how big the chunk is. the biggest be like 4 or 8 or something like that 
    // have pass in the size, face, and where on the face it is
    // do math to make face :D

    public enum Direction {
        Up,
        Down,
        Left,
        Right,
        Forward,
        Back,
    }

    public Chunk(int vertexCount, float radius, int maxSize, int size, Vector2Int pos, Direction direction) {
        _vertexCount = vertexCount;

        var subScale = maxSize - size; // not a great name

        GenerateMesh(vertexCount, radius, subScale, pos, direction);
        CreateObject();
    }

    private void GenerateMesh(int vertexCount, float radius, int size, Vector2Int pos, Direction direction) {
        _mesh = new Mesh();

        _vertices = new Vector3[vertexCount * vertexCount];
        _tris = new int[(vertexCount - 1) * vertexCount * vertexCount *
                        6]; // ngl i don't get this but it's also what makes sense when assigning them

        _uvs = new Vector2[vertexCount * vertexCount];
        _normals = new Vector3[vertexCount * vertexCount];

        var (corner1, increment1, increment2) = GetDirectionInformation(direction);

        var sizeIncrement1 = increment1 * 2 / Mathf.Pow(2, size);
        var sizeIncrement2 = increment2 * 2 / Mathf.Pow(2, size);

        corner1 += sizeIncrement1 * pos.x + sizeIncrement2 * pos.y;

        increment1 = increment1 / (vertexCount - 1) * 2 / Mathf.Pow(2, size);
        increment2 = increment2 / (vertexCount - 1) * 2 / Mathf.Pow(2, size);

        for (var i = 0; i < vertexCount; i++) {
            for (var j = 0; j < vertexCount; j++) {
                var vertex = corner1 + increment1 * i + increment2 * j;

                var normalizedVertex = vertex.normalized;

                vertex = normalizedVertex * radius;

                _normals[PosToIndex(i, j)] = normalizedVertex;
                _vertices[PosToIndex(i, j)] = vertex;

                _uvs[PosToIndex(i, j)] = new Vector2(i / (vertexCount - 1f), j / (vertexCount - 1f));
            }
        }

        for (var i = 0; i < vertexCount - 1; i++) {
            for (var j = 0; j < vertexCount - 1; j++) {
                var startingIndex = (j * vertexCount + i) * 6;

                _tris[startingIndex] = PosToIndex(i, j);
                _tris[startingIndex + 1] = PosToIndex(i + 1, j);
                _tris[startingIndex + 2] = PosToIndex(i, j + 1);

                _tris[startingIndex + 3] = PosToIndex(i + 1, j);
                _tris[startingIndex + 4] = PosToIndex(i + 1, j + 1);
                _tris[startingIndex + 5] = PosToIndex(i, j + 1);
            }
        }

        _mesh.SetVertices(_vertices);
        _mesh.SetNormals(_normals);
        _mesh.SetUVs(0, _uvs);
        _mesh.SetTriangles(_tris, 0);
    }

    private void CreateObject() {
        gameObject = new GameObject("Chunk");

        gameObject.AddComponent<MeshFilter>();
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        gameObject.AddComponent<MeshCollider>();

        gameObject.GetComponent<MeshFilter>().mesh = _mesh;
        gameObject.GetComponent<MeshCollider>().sharedMesh = _mesh;
    }

    private (Vector3, Vector3, Vector3) GetDirectionInformation(Direction direction) {
        Vector3 corner1;
        Vector3 corner2;
        Vector3 dir1;
        Vector3 dir2;

        switch (direction) {
            case Direction.Up:
                corner1 = new Vector3(1, 1, 1);
                corner2 = new Vector3(-1, 1, -1);
                dir1 = new Vector3(0, 0, -1);
                dir2 = new Vector3(-1, 0, 0);
                break;
            case Direction.Down:
                corner1 = new Vector3(-1, -1, 1);
                corner2 = new Vector3(1, -1, -1);
                dir1 = new Vector3(0, 0, -1);
                dir2 = new Vector3(1, 0, 0);
                break;
            case Direction.Left:
                corner1 = new Vector3(-1, 1, 1);
                corner2 = new Vector3(-1, -1, -1);
                dir1 = new Vector3(0, 0, -1);
                dir2 = new Vector3(0, -1, 0);
                break;
            case Direction.Right:
                corner1 = new Vector3(1, 1, -1);
                corner2 = new Vector3(1, -1, 1);
                dir1 = new Vector3(0, 0, 1);
                dir2 = new Vector3(0, -1, 0);
                break;
            case Direction.Forward:
                corner1 = new Vector3(1, 1, 1);
                corner2 = new Vector3(-1, -1, 1);
                dir1 = new Vector3(-1, 0, 0);
                dir2 = new Vector3(0, -1, 0);
                break;
            case Direction.Back:
                corner1 = new Vector3(-1, 1, -1);
                corner2 = new Vector3(1, -1, -1);
                dir1 = new Vector3(1, 0, 0);
                dir2 = new Vector3(0, -1, 0);
                break;
            default:
                corner1 = new Vector3(1, 1, 1);
                corner2 = new Vector3(-1, -1, 1);
                dir1 = new Vector3(-1, 0, 0);
                dir2 = new Vector3(0, -1, 0);
                break;
        }

        return (corner1, dir1, dir2);
    }

    private int PosToIndex(int x, int y) {
        return y * _vertexCount + x;
    }
}