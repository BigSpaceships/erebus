using System;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.Serialization;

public class FlatPlanet : MonoBehaviour {
    public Material planetMaterial;

    public int numberOfVertices;
    public float size;

    public Dictionary<Vector2Int, FlatChunk> chunks;

    [SerializeField] private Transform chunkTransform;

    public float[] lodDists;

    [Header("Water Settings")] // waaa comment to make formatting nice
    public float waveScale;

    public float waveScale2;
    public float waveHeight;

    private Transform _cameraTransform;

    private void Awake() {
        _cameraTransform = Camera.main.transform;

        chunkTransform = transform;

        // GenerateChunk();
        if (Application.isPlaying) {
            Generate();
        }
    }

    public void ClearChildren() {
        for (int i = 0; i < chunkTransform.childCount; i++) {
            if (Application.isPlaying) {
                Destroy(chunkTransform.GetChild(0).gameObject);
            }
            else {
                DestroyImmediate(chunkTransform.GetChild(0).gameObject);
            }
        }
        // while (chunkTransform.childCount > 0) {
        //     if (Application.isPlaying) {
        //         Destroy(chunkTransform.GetChild(0).gameObject);
        //     }
        //     else {
        //         DestroyImmediate(chunkTransform.GetChild(0).gameObject);
        //     }
        // }
    }

    void Update() {
        if (Application.isPlaying) {
            foreach (var chunk in chunks) {
                chunk.Value.meshRenderer.material.SetFloat("_WaveScale", waveScale);
                chunk.Value.meshRenderer.material.SetFloat("_WaveScale2", waveScale2);
                chunk.Value.meshRenderer.material.SetFloat("_WaveHeight", waveHeight);
                chunk.Value.meshRenderer.material.SetFloat("_WaveTime", Time.time);
            }
        }
    }

    private void InsertChunk(Vector2Int pos, FlatChunk chunk) {
        chunks[pos] = chunk;
    }

    public void Generate() {
        ClearChildren();

        chunks = new Dictionary<Vector2Int, FlatChunk>();

        var chunk = new FlatChunk(numberOfVertices, size, Vector2Int.zero);

        chunk.gameObject.transform.parent = chunkTransform;
        chunk.gameObject.GetComponent<MeshRenderer>().material = planetMaterial;

        InsertChunk(Vector2Int.zero, chunk);
    }

    public void GenerateSpectrumTexture() {
        
    }

    // public void GenerateChunk() {
    //     ClearChildren();

    //     chunks = new Dictionary<Vector2Int, FlatChunk>();

    //     var chunk = new FlatChunk(numberOfVertices, size);

    //     chunk.gameObject.transform.parent = chunkTransform;
    //     chunk.gameObject.GetComponent<MeshRenderer>().material = planetMaterial;

    //     InsertChunk(Vector2Int.zero, chunk);
    // }

    private void OnDrawGizmosSelected() {
        if (_cameraTransform == null) {
            _cameraTransform = Camera.main.transform;
        }

        foreach (var dist in lodDists) {
            Gizmos.DrawWireSphere(_cameraTransform.position, dist);
        }
    }
}