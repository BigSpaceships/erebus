using System;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.Serialization;

public class Planet : MonoBehaviour {
    public Material planetMaterial;

    public int numberOfVertices;
    public float radius;

    public int maxSize;

    public float[] lodDists;

    // first array is size, then direction, and then a map for the positions
    public Dictionary<Vector2Int, Chunk>[,] chunks;

    [Header("Single Chunk")] public int size;
    public Vector2Int chunkPlace;
    public Chunk.Direction direction;

    [Header("Water Settings")] // waaa comment to make formatting nice
    public float waveScale;

    public float waveScale2;
    public float waveHeight;

    [HideInInspector] public Transform[] sizeTransforms;

    private Transform _cameraTransform;

    private void ClearChildren() {
        sizeTransforms = new Transform[maxSize + 1];

        foreach (var size in sizeTransforms) {
            if (size == null) continue;

            while (size.childCount > 0) {
                DestroyImmediate(transform.GetChild(0).gameObject);
            }
        }

        while (transform.childCount > 0) {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }

        chunks = new Dictionary<Vector2Int, Chunk>[maxSize, 6];
    }

    private void Awake() {
        _cameraTransform = Camera.main.transform;

        GenerateChunk();
        // Generate();
    }

    void Update() {
        for (int i = 0; i < chunks.GetLength(0); i++) {
            for (int j = 0; j < chunks.GetLength(1); j++) {
                if (chunks[i, j] == null) {
                    continue;
                }

                foreach (var chunk in chunks[i, j]) {
                    chunk.Value.meshRenderer.material.SetFloat("_WaveScale", waveScale);
                    chunk.Value.meshRenderer.material.SetFloat("_WaveScale2", waveScale2);
                    chunk.Value.meshRenderer.material.SetFloat("_WaveHeight", waveHeight);
                    chunk.Value.meshRenderer.material.SetFloat("_WaveTime", Time.time);
                }
            }
        }
    }

    private void InsertChunk(int size, Chunk.Direction dir, Vector2Int pos, Chunk chunk) {
        if (chunks[size, (int)dir] == null) {
            chunks[size, (int)dir] = new Dictionary<Vector2Int, Chunk>();
        }

        chunks[size, (int)dir][pos] = chunk;
    }

    private void SetupSizes() {
        for (int i = 0; i < maxSize + 1; i++) {
            var parent = new GameObject($"Size: {i}");
            parent.transform.parent = transform;

            sizeTransforms[i] = parent.transform;
        }
    }

    public void Generate() {
        ClearChildren();

        SetupSizes();

        chunks = new Dictionary<Vector2Int, Chunk>[maxSize + 1, 6];

        foreach (Chunk.Direction dir in Enum.GetValues(typeof(Chunk.Direction))) {
            var chunk = new Chunk(numberOfVertices, radius, maxSize, maxSize, Vector2Int.zero, dir);

            chunk.gameObject.transform.parent = sizeTransforms[maxSize];
            chunk.gameObject.GetComponent<MeshRenderer>().material = planetMaterial;

            InsertChunk(maxSize, dir, Vector2Int.zero, chunk);
        }
    }

    public void GenerateChunk() {
        ClearChildren();

        SetupSizes();

        chunks = new Dictionary<Vector2Int, Chunk>[maxSize + 1, 6];

        var chunk = new Chunk(numberOfVertices, radius, maxSize, size, chunkPlace, direction);

        chunk.gameObject.transform.parent = sizeTransforms[size];
        chunk.gameObject.GetComponent<MeshRenderer>().material = planetMaterial;

        InsertChunk(size, direction, Vector2Int.zero, chunk);
    }

    private void OnDrawGizmosSelected() {
        if (_cameraTransform == null) {
            _cameraTransform = Camera.main.transform;
        }

        foreach (var dist in lodDists) {
            Gizmos.DrawWireSphere(_cameraTransform.position, dist);
        }
    }
}