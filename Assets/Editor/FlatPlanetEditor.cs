using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FlatPlanet))]
    public class FlatPlanetEditor : Editor {
        public override void OnInspectorGUI() {
            DrawDefaultInspector();

            if (GUILayout.Button("Generate")) {
                ((FlatPlanet) target).Generate();
            }
        }
    }