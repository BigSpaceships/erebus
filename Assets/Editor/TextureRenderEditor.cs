using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TextureRenderer))]
public class TextureRenderEditor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        if (GUILayout.Button("Generate")) {
            ((TextureRenderer) target).Generate();
        }
        
        if (GUILayout.Button("Generate Noise")) {
            ((TextureRenderer) target).GenerateGaussianTexture();
        }
    }
}