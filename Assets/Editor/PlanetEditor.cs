using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Planet))]
    public class PlanetEditor : Editor {
        public override void OnInspectorGUI() {
            DrawDefaultInspector();

            if (GUILayout.Button("Generate")) {
                ((Planet) target).Generate();
            }

            if (GUILayout.Button("Generate Specific")) {
                ((Planet) target).GenerateChunk();
            }
        }
    }