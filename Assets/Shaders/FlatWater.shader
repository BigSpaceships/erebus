Shader "Unlit/FlatWater"
{
    Properties
    {
        _WaveHeight ("Wave Height", Float) = 0
        _WaveScale ("Wave Scale", Float) = 0
        _WaveScale2 ("Wave Scale2", Float) = 0
        _WaveTime ("Wave Time", Float) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
        }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 normal : NORMAL;
            };

            struct vertdata
            {
                float theta;
                float phi;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float4 normal : NORMAL;
                fixed4 col : COLOR;
            };

            float _WaveHeight;
            float _WaveScale;
            float _WaveScale2;
            float _WaveTime;

            float getheight(float4 v)
            {
                float height = v.x;

                return _WaveHeight * height;
            }

            float4 getnormals(float4 v)
            {
                float3 normals = float3(0.0, 0.0, 0.0);

                return float4(normals, 0.0);
            }

            v2f vert(appdata v)
            {
                v2f o;
                float4 vertex = v.vertex;

                vertex.y += getheight(vertex);

                o.col = fixed4(0.0, 0.0, 1.0, 1.0);
                o.normal = getnormals(vertex); // fuckkkk normals are going to suck ASS

                vertex = UnityObjectToClipPos(vertex);

                o.vertex = vertex;
                o.uv = v.uv;
                //o.normal = v.normal;
                UNITY_TRANSFER_FOG(o, o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                // sample the texture
                UNITY_APPLY_FOG(i.fogCoord, col);
                return i.col;
            }
            ENDCG
        }
    }
}