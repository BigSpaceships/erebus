Shader "Unlit/Water"
{
    Properties
    {
        _WaveHeight ("Wave Height", Float) = 0
        _WaveScale ("Wave Scale", Float) = 0
        _WaveScale2 ("Wave Scale2", Float) = 0
        _WaveTime ("Wave Time", Float) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
        }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 normal : NORMAL;
            };

            struct vertdata
            {
                float theta;
                float phi;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float4 normal : NORMAL;
                fixed4 col : COLOR;
            };

            float _WaveHeight;
            float _WaveScale;
            float _WaveScale2;
            float _WaveTime;

            float3 getangles(float4 v)
            {
                // TODO: maybe do this from places besides the axis
                // TODO: i think i could dot product
                float one = acos(v.x / length(v.xyz)) / UNITY_HALF_PI;

                float two = acos(v.y / length(v.xyz)) / UNITY_HALF_PI;

                float three = acos(v.z / length(v.xyz)) / UNITY_HALF_PI;

                return float3(one, two, three);
            }

            float getheight(float4 v)
            {
                float3 angles = getangles(v);

                float height = 0;
                for (int i = 0; i < 1; ++i)
                {
                    height += sin((_WaveScale * angles.x + _WaveTime) * (2 ^ i));
                    // height += sin((_WaveScale * angles.y + _WaveTime) * (2 ^ i));
                    // height += sin((_WaveScale * angles.z + _WaveTime) * (2 ^ i));
                }

                return _WaveHeight * height;
            }

            float4 getnormals(float4 v)
            {
                float3 angles = getangles(v);

                float3 normals = float3(0.0, 0.0, 0.0);

                float x2y2 = v.x * v.x + v.y * v.y;
                float x2z2 = v.x * v.x + v.z * v.z;
                float y2z2 = v.y * v.y + v.z * v.z;

                float len = length(v);

                float changex = -sqrt(y2z2) / len;
                float changey = -sqrt(x2z2) / len;
                float changez = -sqrt(x2y2) / len;

                for (int i = 0; i < 1; ++i)
                {
                    float changetheta = (2 ^ i) * _WaveScale * cos(angles.x * _WaveScale + _WaveTime);

                    normals += float3(changex, changey, changez) * changetheta;
                }

                return float4(normals, 0.0);
            }

            v2f vert(appdata v)
            {
                v2f o;
                float4 vertex = v.vertex;

                vertex = normalize(vertex) * (length(vertex) + getheight(vertex));

                o.col = fixed4(getangles(vertex), 1.0);
                o.normal = getnormals(vertex); // fuckkkk normals are going to suck ASS

                vertex = UnityObjectToClipPos(vertex);

                o.vertex = vertex;
                o.uv = v.uv;
                //o.normal = v.normal;
                UNITY_TRANSFER_FOG(o, o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                // sample the texture
                UNITY_APPLY_FOG(i.fogCoord, col);
                return i.normal;
            }
            ENDCG
        }
    }
}